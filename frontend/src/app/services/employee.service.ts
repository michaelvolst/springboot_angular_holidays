import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Employee } from '../models/Employee';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};
@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  private endpoint: string = 'api/employees';

  constructor(private http: HttpClient) {}

  getEmployees(): Observable<Employee[]> {
    return this.http.get<Employee[]>(this.endpoint);
  }

  addEmployee(employee: Employee): Observable<Employee> {
    return this.http.post<Employee>(this.endpoint, employee, httpOptions);
  }

  updateEmployee(employee: Employee): Observable<Employee> {
    return this.http.put<Employee>(`${this.endpoint}/${employee.id}`, employee, httpOptions);
  }

  deleteEmployee(employee: Employee): Observable<Employee> {
    return this.http.delete<Employee>(`${this.endpoint}/${employee.id}`, httpOptions);
  }
}
