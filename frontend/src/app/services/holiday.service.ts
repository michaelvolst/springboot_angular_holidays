import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Holiday } from '../models/Holiday';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};
@Injectable({
  providedIn: 'root'
})
export class HolidayService {
  private endpoint: string = 'api/holidays';

  constructor(private http: HttpClient) {}

  getHolidays(): Observable<Holiday[]> {
    return this.http.get<Holiday[]>(this.endpoint);
  }

  addHoliday(holiday: Holiday): Observable<Holiday> {
    return this.http.post<Holiday>(this.endpoint, holiday, httpOptions);
  }

  updateHoliday(holiday: Holiday): Observable<Holiday> {
    return this.http.put<Holiday>(`${this.endpoint}/${holiday.id}`, holiday, httpOptions);
  }

  deleteHoliday(holiday: Holiday): Observable<Holiday> {
    return this.http.delete<Holiday>(`${this.endpoint}/${holiday.id}`, httpOptions);
  }
}
