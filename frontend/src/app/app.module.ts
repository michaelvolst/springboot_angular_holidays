import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { ApiInterceptor } from './interceptors/ApiInterceptor';

import { AppComponent } from './app.component';
import { EmployeesComponent } from './components/employees/employees.component';

import { HolidaysComponent } from './components/holidays/holidays.component';
import { LogoComponent } from './components/logo/logo.component';
import { MenuComponent } from './components/menu/menu.component';
import { EmployeeFormComponent } from './components/employee-form/employee-form.component';
import { HolidayFormComponent } from './components/holiday-form/holiday-form.component';

const appRoutes: Routes = [
  { path: '', component: EmployeesComponent },
  { path: 'holidays', component: HolidaysComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    EmployeesComponent,
    HolidaysComponent,
    LogoComponent,
    MenuComponent,
    EmployeeFormComponent,
    HolidayFormComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes, { enableTracing: true }),
    HttpClientModule,
    ReactiveFormsModule
  ],
  exports: [
    RouterModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiInterceptor,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
