export interface Employee {
  id: string;
  employeeId: string;
  name: string;
}