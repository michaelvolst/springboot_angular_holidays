export enum HolidayStates {
  DRAFT,
  REQUESTED,
  SCHEDULED,
  ARCHIVED,
}
