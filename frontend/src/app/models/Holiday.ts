import { Employee } from "./Employee"
import { HolidayStates } from "./HolidayStates";

export interface Holiday {
  id: string;
  label: string;
  startOfHoliday: String;
  endOfHoliday: String;
  status: HolidayStates;
  employee: Employee;
}

