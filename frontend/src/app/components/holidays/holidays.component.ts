import { Component, OnInit } from '@angular/core';
import { Holiday } from 'src/app/models/Holiday';
import { HolidayStates } from 'src/app/models/HolidayStates';
import { HolidayService } from 'src/app/services/holiday.service';
@Component({
  selector: 'app-holidays',
  templateUrl: './holidays.component.html',
  styleUrls: ['./holidays.component.css']
})
export class HolidaysComponent implements OnInit {

  modalOpen: boolean = false
  holidays: Holiday[] = []
  currentHoliday!: Holiday

  constructor(private holidayService: HolidayService) {}

  ngOnInit(): void {
    this.holidayService.getHolidays().subscribe(holidays => (this.holidays = holidays))
    this.resetHolidayState()
  }

  resetHolidayState () {
    this.currentHoliday = {
      id: "",
      label: "",
      startOfHoliday: "",
      endOfHoliday: "",
      status: HolidayStates.DRAFT,
      employee: {
        id: "",
        employeeId: "",
        name: ""
      }
    }
  }

  modalState(state: boolean): void {
    this.modalOpen = state
  }

  addHoliday(holiday: Holiday) {
    this.holidayService.addHoliday(holiday).subscribe(
      holiday => {
        this.holidays.push(holiday)
        this.modalOpen = false
      },
      error => alert(error.error)
    )
  }

  editHoliday(holiday: Holiday) {
    this.currentHoliday = holiday
    this.modalOpen = true
  }

  updateHoliday(holiday: Holiday) {
    this.holidayService.updateHoliday(holiday).subscribe(
      updatedHoliday => {

        let index = this.holidays.findIndex(h => h.id === updatedHoliday.id)
        this.holidays[index] = updatedHoliday

        this.cancelHoliday()
      },
      error => alert(error.error)
    )
  }

  deleteHoliday(holiday: Holiday) {

    if(!confirm("Are you sure?")) {
      return
    }

    this.holidayService.deleteHoliday(holiday).subscribe(
      () => this.holidays = this.holidays.filter((h) => h.id !== holiday.id),
      error => alert(error.error)
    )
  }

  cancelHoliday() {
    this.modalOpen = false
    this.resetHolidayState()
  }

}
