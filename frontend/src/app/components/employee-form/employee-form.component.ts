import { Component, OnInit, Output,EventEmitter, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Employee } from 'src/app/models/Employee';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.css']
})
export class EmployeeFormComponent implements OnInit {

  @Output() onAddEmployee: EventEmitter<Employee> = new EventEmitter()
  @Output() onUpdateEmployee: EventEmitter<Employee> = new EventEmitter()
  @Output() onCancelEmployee: EventEmitter<boolean> = new EventEmitter()

  @Input() employee!: Employee

  employeeForm!: FormGroup;

  constructor() {}

  ngOnInit() {
    this.employeeForm = new FormGroup({
      name: new FormControl(this.employee?.name,  [
        Validators.required,
        Validators.minLength(3),
      ]),
      employeeId: new FormControl(this.employee?.employeeId, [
        Validators.required,
        Validators.minLength(9),
        Validators.pattern("^klm[0-9]{6}$")
      ]),
    })
  }

  cancelEmployee(): void {
    this.onCancelEmployee.emit()
  }

  submit() {
    if(this.employee.id) {
      this.updateEmployee()
      return
    }

    this.createEmployee();
  }

  updateEmployee() {
    this.onUpdateEmployee.emit({
      id: this.employee.id,
      name: this.employeeForm.get("name")?.value,
      employeeId: this.employeeForm.get("employeeId")?.value,
    })
  }

  createEmployee() {
    const newEmployee: Employee = {
      id: "",
      name: this.employeeForm.get("name")?.value,
      employeeId: this.employeeForm.get("employeeId")?.value,
    }

    this.onAddEmployee.emit(newEmployee)
  }

  get name() { return this.employeeForm.get('name'); }

  get employeeId() { return this.employeeForm.get('employeeId'); }

}
