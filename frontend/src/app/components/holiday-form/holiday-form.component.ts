import { Component, OnInit, Output,EventEmitter, Input } from '@angular/core';
import { Holiday } from 'src/app/models/Holiday';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Employee } from 'src/app/models/Employee';
import { EmployeeService } from 'src/app/services/employee.service';
import { HolidayStates } from 'src/app/models/HolidayStates';

@Component({
  selector: 'app-holiday-form',
  templateUrl: './holiday-form.component.html',
  styleUrls: ['./holiday-form.component.css']
})
export class HolidayFormComponent implements OnInit {

  @Output() onAddHoliday: EventEmitter<Holiday> = new EventEmitter()
  @Output() onUpdateHoliday: EventEmitter<Holiday> = new EventEmitter()
  @Output() onCancelHoliday: EventEmitter<boolean> = new EventEmitter()

  @Input() holiday!: Holiday

  holidayForm!: FormGroup;
  employees: Employee[] = [];

  constructor(private employeeService: EmployeeService) {}

  ngOnInit(): void {

    this.employeeService.getEmployees().subscribe(employees => (this.employees = employees))

    this.holidayForm = new FormGroup({
      label: new FormControl(this.holiday?.label,  [
        Validators.required,
      ]),
      startOfHoliday: new FormControl(this.holiday?.startOfHoliday, [
        Validators.required,
        // Validators.pattern("/^\d{1,2}\.\d{1,2}\.\d{4}$/"),
      ]),
      endOfHoliday: new FormControl(this.holiday?.endOfHoliday, [
        Validators.required,
        // Validators.pattern("\d{2}-\d{2}-\d{4}"),
      ]),
      status: new FormControl(this.holiday?.status, [
        Validators.required,
      ]),
      employee: new FormControl(this.holiday?.employee.id, [
        Validators.required,
      ]),
    })
  }

  getHolidayStates() : Array<string> {
    const keys = Object.keys(HolidayStates);
    return keys.slice(keys.length / 2);
  }

  cancelHoliday(): void {
    this.onCancelHoliday.emit()
  }

  submit() {
    if(this.holiday.id) {
      this.updateHoliday()
      return
    }

    this.createHoliday();
  }

  updateHoliday() {

    let employeeIndex = this.employees.findIndex(e => e.id === this.holidayForm.get('employee')?.value)

    this.onUpdateHoliday.emit({
      id: this.holiday.id,
      label: this.holidayForm.get('label')?.value,
      startOfHoliday: this.holidayForm.get('startOfHoliday')?.value,
      endOfHoliday: this.holidayForm.get('endOfHoliday')?.value,
      status: this.holidayForm.get('status')?.value,
      employee: this.employees[employeeIndex],
    })
  }

  createHoliday() {
    let employeeIndex = this.employees.findIndex(e => e.id === this.holidayForm.get('employee')?.value)

    const newHoliday: Holiday = {
      id: "",
      label: this.holidayForm.get('label')?.value,
      startOfHoliday: this.holidayForm.get('startOfHoliday')?.value,
      endOfHoliday: this.holidayForm.get('endOfHoliday')?.value,
      status: this.holidayForm.get('status')?.value,
      employee: this.employees[employeeIndex],
    }

    this.onAddHoliday.emit(newHoliday)
  }

  get label() { return this.holidayForm.get('label'); }

  get startOfHoliday() { return this.holidayForm.get('startOfHoliday'); }

  get endOfHoliday() { return this.holidayForm.get('endOfHoliday'); }

  get status() { return this.holidayForm.get('status'); }

  get employee() { return this.holidayForm.get('employee'); }


}
