import { Component, OnInit } from '@angular/core';
import { Employee } from 'src/app/models/Employee';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {

  modalOpen: boolean = false
  employees: Employee[] = []
  currentEmployee!: Employee

  constructor(private employeeService: EmployeeService) {}

  ngOnInit(): void {
    this.employeeService.getEmployees().subscribe(employees => (this.employees = employees))
    this.resetEmployeeState()
  }

  resetEmployeeState () {
    this.currentEmployee = {
      id: "",
      name: '',
      employeeId: '',
    }
  }

  modalState(state: boolean): void {
    this.modalOpen = state
  }

  addEmployee(employee: Employee) {
    this.employeeService.addEmployee(employee).subscribe(
      employee => {
        this.employees.push(employee)
        this.cancelEmployee()
      },
      error =>  {
        alert(error.error)
      }
    )
  }

  editEmployee(employee: Employee) {
    this.currentEmployee = employee
    this.modalOpen = true
  }

  updateEmployee(employee: Employee) {
    this.employeeService.updateEmployee(employee).subscribe(
      updatedEmployee => {

        let employeeIndex = this.employees.findIndex(employee => employee.id === updatedEmployee.id)
        this.employees[employeeIndex] = updatedEmployee

        this.cancelEmployee()
      },
      error =>  {
        alert(error.error)
      }
    )
  }

  deleteEmployee(employee: Employee) {

    if(!confirm("Are you sure?")) {
      return
    }

    this.employeeService.deleteEmployee(employee).subscribe(
      () => this.employees = this.employees.filter((e) => e.id !== employee.id),
      error => alert(error.error)
    )
  }

  cancelEmployee() {
    this.modalOpen = false
    this.resetEmployeeState()
  }

}
