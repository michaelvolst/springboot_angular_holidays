package com.airfranceklm.fasttrack.assignment.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.airfranceklm.fasttrack.assignment.mapper.HolidayMapper;
import com.airfranceklm.fasttrack.assignment.dto.HolidayResponseDto;
import com.airfranceklm.fasttrack.assignment.service.HolidayService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static java.util.stream.Collectors.toList;

@AllArgsConstructor
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class HolidayController {

    private final HolidayService holidayService;

    @GetMapping("/api/holidays")
    public ResponseEntity<List<HolidayResponseDto>> getAllHolidays() {
        List<HolidayResponseDto> holidays = this.holidayService.all()
                .stream()
                .map(HolidayMapper::toDto).collect(toList());
        return ResponseEntity.status(HttpStatus.OK).body(holidays);
    }

    @PostMapping("/api/holidays")
    public ResponseEntity<HolidayResponseDto> createHoliday(@Valid @RequestBody HolidayResponseDto newHoliday) {
        return ResponseEntity.status(HttpStatus.OK).body(
                HolidayMapper.toDto(this.holidayService.create(newHoliday))
        );
    }

    @PutMapping("/api/holidays/{uuid}")
    public ResponseEntity<HolidayResponseDto> updateHoliday(@Valid @RequestBody HolidayResponseDto updatedHoliday) {
        return ResponseEntity.status(HttpStatus.OK).body(
                HolidayMapper.toDto(this.holidayService.update(updatedHoliday))
        );
    }

    @DeleteMapping("/api/holidays/{uuid}")
    public ResponseEntity<String> deleteHoliday(@PathVariable String uuid) {
        this.holidayService.delete(UUID.fromString(uuid));
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

}
