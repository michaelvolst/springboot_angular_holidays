package com.airfranceklm.fasttrack.assignment.controller;

import com.airfranceklm.fasttrack.assignment.model.Employee;
import com.airfranceklm.fasttrack.assignment.mapper.EmployeeMapper;
import com.airfranceklm.fasttrack.assignment.dto.EmployeeRequestDto;
import com.airfranceklm.fasttrack.assignment.dto.EmployeeResponseDto;
import com.airfranceklm.fasttrack.assignment.service.implementations.EmployeeServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

import static java.util.stream.Collectors.toList;

@AllArgsConstructor
@RestController
@RequestMapping("/api/employees")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class EmployeesController {

    private final EmployeeServiceImpl employeeService;

    @GetMapping(path = "")
    public ResponseEntity<List<EmployeeResponseDto>> getEmployees() {
        List<EmployeeResponseDto> employees = this.employeeService.all()
                .stream()
                .map(EmployeeMapper::toDto).collect(toList());
        return ResponseEntity.status(HttpStatus.OK).body(employees);
    }


    @GetMapping("/{uuid}")
    public ResponseEntity<EmployeeResponseDto> showEmployee(@PathVariable String uuid) {
        UUID convertedUuid = UUID.fromString(uuid);
        return ResponseEntity.status(HttpStatus.OK).body(
                EmployeeMapper.toDto(this.employeeService.get(convertedUuid))
        );
    }

    @PostMapping("")
    public ResponseEntity<EmployeeResponseDto> createEmployee(@Valid @RequestBody Employee newEmployee) {
        return ResponseEntity.status(HttpStatus.OK).body(
                EmployeeMapper.toDto(this.employeeService.create(newEmployee))
        );
    }

    @PutMapping("/{uuid}")
    public ResponseEntity<EmployeeResponseDto> updateEmployee(@Valid @RequestBody EmployeeRequestDto body, @PathVariable String uuid) {
        return ResponseEntity.status(HttpStatus.OK).body(
                EmployeeMapper.toDto(this.employeeService.update(this.employeeService.get(UUID.fromString(uuid)), body))
        );
    }

    @DeleteMapping("/{uuid}")
    public ResponseEntity<String> deleteEmployee(@PathVariable String uuid) {
        this.employeeService.delete(UUID.fromString(uuid));
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

}
