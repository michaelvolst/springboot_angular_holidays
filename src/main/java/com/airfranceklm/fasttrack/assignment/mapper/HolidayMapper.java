package com.airfranceklm.fasttrack.assignment.mapper;

import com.airfranceklm.fasttrack.assignment.dto.HolidayResponseDto;
import com.airfranceklm.fasttrack.assignment.model.Holiday;
import org.springframework.stereotype.Component;

@Component
public class HolidayMapper {
    public static HolidayResponseDto toDto(Holiday holiday) {
        return new HolidayResponseDto(
                holiday.getUuid(),
                holiday.getLabel(),
                holiday.getStartOfHoliday(),
                holiday.getEndOfHoliday(),
                holiday.getStatus(),
                EmployeeMapper.toDto(holiday.getEmployee())
        );
    }


    public static Holiday toHoliday(HolidayResponseDto holidayDto) {
        return new Holiday(
                holidayDto.getLabel(),
                holidayDto.getStartOfHoliday(),
                holidayDto.getEndOfHoliday(),
                holidayDto.getStatus()
        );
    }
}
