package com.airfranceklm.fasttrack.assignment.mapper;

import com.airfranceklm.fasttrack.assignment.dto.EmployeeResponseDto;
import com.airfranceklm.fasttrack.assignment.model.Employee;
import org.springframework.stereotype.Component;

@Component
public class EmployeeMapper {
    public static EmployeeResponseDto toDto(Employee employee) {
        return new EmployeeResponseDto(employee.getUuid(), employee.getEmployeeId(), employee.getName());
    }
}
