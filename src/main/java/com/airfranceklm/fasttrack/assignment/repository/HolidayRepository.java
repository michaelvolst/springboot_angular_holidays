package com.airfranceklm.fasttrack.assignment.repository;

import com.airfranceklm.fasttrack.assignment.model.Holiday;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface HolidayRepository extends JpaRepository<Holiday, Long> {
    Optional<Holiday> findByUuid(UUID uuid);
    int deleteByUuid(UUID uuid);
}
