package com.airfranceklm.fasttrack.assignment.dto;

import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@AllArgsConstructor
public class EmployeeRequestDto {
    private String id;
    private String employeeId;
    private String name;
}
