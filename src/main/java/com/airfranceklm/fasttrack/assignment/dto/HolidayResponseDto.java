package com.airfranceklm.fasttrack.assignment.dto;

import com.airfranceklm.fasttrack.assignment.model.HolidayStatus;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;
import java.util.UUID;

@Data
@AllArgsConstructor
public class HolidayResponseDto {
    private UUID id;
    private String label;
    private LocalDate startOfHoliday;
    private LocalDate endOfHoliday;
    private HolidayStatus status;
    private EmployeeResponseDto employee;
}
