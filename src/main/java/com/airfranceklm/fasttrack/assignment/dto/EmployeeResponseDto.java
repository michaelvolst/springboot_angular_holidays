package com.airfranceklm.fasttrack.assignment.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class EmployeeResponseDto {
    private UUID id;
    private String employeeId;
    private String name;
}
