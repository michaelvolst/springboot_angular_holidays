package com.airfranceklm.fasttrack.assignment.dto;

import com.airfranceklm.fasttrack.assignment.model.HolidayStatus;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class HolidayRequestDto {
    private String id;
    private String label;
    private LocalDate startOfHoliday;
    private LocalDate endOfHoliday;
    private HolidayStatus status;
    private EmployeeRequestDto employee;
}
