package com.airfranceklm.fasttrack.assignment.exceptions;

import java.util.UUID;

public class HolidayNotFoundException extends RuntimeException{
    public HolidayNotFoundException(UUID uuid) {
        super("Could not find a holiday with the id: " + uuid);
    }

}
