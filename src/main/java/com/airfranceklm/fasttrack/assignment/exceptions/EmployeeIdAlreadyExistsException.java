package com.airfranceklm.fasttrack.assignment.exceptions;


public class EmployeeIdAlreadyExistsException extends RuntimeException{

    public EmployeeIdAlreadyExistsException(String employeeId) {
        super("Employee with this employee id " + employeeId + " already exists");
    }

}
