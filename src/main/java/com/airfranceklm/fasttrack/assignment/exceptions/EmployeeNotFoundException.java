package com.airfranceklm.fasttrack.assignment.exceptions;

import java.util.UUID;

public class EmployeeNotFoundException extends RuntimeException{
    public EmployeeNotFoundException(UUID uuid) {
        super("Could not find an employee with the id: " + uuid);
    }
}
