package com.airfranceklm.fasttrack.assignment.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.UUID;

@Data
@Entity
@NoArgsConstructor
public class Holiday {

    @Id @GeneratedValue
    private Long id;

    private String label;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "employee_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Employee employee;

    private UUID uuid = UUID.randomUUID();

    private LocalDate startOfHoliday;

    private LocalDate endOfHoliday;

    private HolidayStatus status;

    public Holiday(String label, LocalDate startOfHoliday, LocalDate endOfHoliday, HolidayStatus status) {
        this.label = label;
        this.startOfHoliday = startOfHoliday;
        this.endOfHoliday = endOfHoliday;
        this.status = status;
    }

}
