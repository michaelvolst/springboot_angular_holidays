package com.airfranceklm.fasttrack.assignment.model;

public enum HolidayStatus {
    DRAFT("DRAFT"),
    REQUESTED("REQUESTED"),
    SCHEDULED("SCHEDULED"),
    ARCHIVED("ARCHIVED");

    private final String status;

    HolidayStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }
}
