package com.airfranceklm.fasttrack.assignment.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import java.util.UUID;

@Data
@Entity
@NoArgsConstructor
public class Employee {

    @Id @GeneratedValue
    private Long id;

    private UUID uuid = UUID.randomUUID();

    @NotBlank(message = "Employee id is required")
    @Pattern(regexp = "^klm[0-9]{6}$", message = "It have to start with klm and end with 6 numbers")
    @Column(unique=true, name= "employeeId")
    private String employeeId;

    @NotBlank(message = "Name is required")
    private String name;

    public Employee(String employeeId, String name) {
        this.name = name;
        this.employeeId = employeeId;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + this.id + ", name='" + this.name + '\'' + ", employeeId='" + this.employeeId + '\'' + '}';
    }
}
