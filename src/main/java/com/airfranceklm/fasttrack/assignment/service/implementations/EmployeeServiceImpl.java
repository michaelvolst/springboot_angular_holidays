package com.airfranceklm.fasttrack.assignment.service.implementations;

import com.airfranceklm.fasttrack.assignment.dto.EmployeeRequestDto;
import com.airfranceklm.fasttrack.assignment.exceptions.EmployeeIdAlreadyExistsException;
import com.airfranceklm.fasttrack.assignment.exceptions.EmployeeNotFoundException;
import com.airfranceklm.fasttrack.assignment.model.Employee;
import com.airfranceklm.fasttrack.assignment.repository.EmployeeRepository;
import com.airfranceklm.fasttrack.assignment.service.EmployeeService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@AllArgsConstructor
@Service
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;

    @Override
    public List<Employee> all() {
        return this.employeeRepository.findAll();
    }

    @Override
    public Employee get(UUID uuid) throws EmployeeNotFoundException {
        return this.employeeRepository.findByUuid(uuid).orElseThrow(() -> new EmployeeNotFoundException(uuid));
    }

    @Override
    public Employee create(Employee employee) throws EmployeeIdAlreadyExistsException {

        if ( this.employeeRepository.findByEmployeeId(employee.getEmployeeId()).isPresent() ) {
            throw new EmployeeIdAlreadyExistsException(employee.getEmployeeId());
        }

        return this.employeeRepository.save(employee);
    }

    @Override
    public Employee update(Employee currentEmployee, EmployeeRequestDto updatedEmployee) throws EmployeeIdAlreadyExistsException {

        if (!Objects.equals(currentEmployee.getEmployeeId(), updatedEmployee.getEmployeeId()) && this.employeeRepository.findByEmployeeId(updatedEmployee.getEmployeeId()).isPresent()) {
            throw new EmployeeIdAlreadyExistsException(updatedEmployee.getEmployeeId());
        }

        currentEmployee.setName(updatedEmployee.getName());
        currentEmployee.setEmployeeId(updatedEmployee.getEmployeeId());
        return this.employeeRepository.save(currentEmployee);
    }

    @Override
    @Transactional
    public Integer delete(UUID uuid) {
        return this.employeeRepository.deleteByUuid(uuid);
    }
}
