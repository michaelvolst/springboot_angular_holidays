package com.airfranceklm.fasttrack.assignment.service;

import com.airfranceklm.fasttrack.assignment.model.Employee;
import com.airfranceklm.fasttrack.assignment.dto.EmployeeRequestDto;

import java.util.List;
import java.util.UUID;

public interface EmployeeService {
    List<Employee> all();
    Employee get(UUID uuid);
    Employee create(Employee employee);
    Employee update(Employee currentEmployee, EmployeeRequestDto updatedEmployee);
    Integer delete(UUID uuid);
}
