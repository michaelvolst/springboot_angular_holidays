package com.airfranceklm.fasttrack.assignment.service;

import com.airfranceklm.fasttrack.assignment.model.Holiday;
import com.airfranceklm.fasttrack.assignment.dto.HolidayResponseDto;

import java.util.List;
import java.util.UUID;

public interface HolidayService {
    List<Holiday> all();
    Holiday get(UUID uuid);
    Holiday create(HolidayResponseDto holiday);
    Holiday update(HolidayResponseDto updatedHoliday);
    Integer delete(UUID uuid);
}
