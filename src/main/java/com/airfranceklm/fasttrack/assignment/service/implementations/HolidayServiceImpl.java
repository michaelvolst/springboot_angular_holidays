package com.airfranceklm.fasttrack.assignment.service.implementations;

import com.airfranceklm.fasttrack.assignment.mapper.HolidayMapper;
import com.airfranceklm.fasttrack.assignment.model.Employee;
import com.airfranceklm.fasttrack.assignment.model.Holiday;
import com.airfranceklm.fasttrack.assignment.repository.HolidayRepository;
import com.airfranceklm.fasttrack.assignment.service.EmployeeService;
import com.airfranceklm.fasttrack.assignment.dto.HolidayResponseDto;
import com.airfranceklm.fasttrack.assignment.exceptions.HolidayNotFoundException;
import com.airfranceklm.fasttrack.assignment.service.HolidayService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@Service
public class HolidayServiceImpl implements HolidayService {

    private final HolidayRepository holidayRepository;
    private final EmployeeService employeeService;

    @Override
    public List<Holiday> all() {
        return this.holidayRepository.findAll();
    }

    @Override
    public Holiday get(UUID uuid) {
        return this.holidayRepository.findByUuid(uuid).orElseThrow(() -> new HolidayNotFoundException(uuid));
    }

    @Override
    public Holiday create(HolidayResponseDto holiday) {

        Employee employee = this.employeeService.get(holiday.getEmployee().getId());

        Holiday newHoliday = HolidayMapper.toHoliday(holiday);
        newHoliday.setEmployee(employee);

        return this.holidayRepository.save(newHoliday);
    }

    @Override
    public Holiday update(HolidayResponseDto updatedHoliday) {

        Employee employee = this.employeeService.get(updatedHoliday.getEmployee().getId());

        Holiday currentHoliday = this.get(updatedHoliday.getId());
        currentHoliday.setLabel(updatedHoliday.getLabel());
        currentHoliday.setStartOfHoliday(updatedHoliday.getStartOfHoliday());
        currentHoliday.setEndOfHoliday(updatedHoliday.getEndOfHoliday());
        currentHoliday.setEmployee(employee);

        return this.holidayRepository.save(currentHoliday);
    }

    @Override
    @Transactional
    public Integer delete(UUID uuid) {
        return this.holidayRepository.deleteByUuid(uuid);
    }
}
