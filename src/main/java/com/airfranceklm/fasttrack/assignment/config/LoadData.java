package com.airfranceklm.fasttrack.assignment.config;

import com.airfranceklm.fasttrack.assignment.model.Employee;
import com.airfranceklm.fasttrack.assignment.repository.EmployeeRepository;
import com.airfranceklm.fasttrack.assignment.model.HolidayStatus;
import com.airfranceklm.fasttrack.assignment.model.Holiday;
import com.airfranceklm.fasttrack.assignment.repository.HolidayRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;

@Configuration
public class LoadData {

    private static final Logger log = LoggerFactory.getLogger(LoadData.class);

    @Bean
    CommandLineRunner init (EmployeeRepository employeeRepository, HolidayRepository holidayRepository) {

        return args -> {
            log.info("Inserting initial data");

            Employee employee1 = new Employee("klm000001", "Michael van Olst");
            employeeRepository.save(employee1);

            Holiday holiday1 = new Holiday("Summer", LocalDate.parse("2021-07-01"), LocalDate.parse("2021-07-15"), HolidayStatus.SCHEDULED);
            holiday1.setEmployee(employee1);
            holidayRepository.save(holiday1);

            Employee employee2 = new Employee("klm000002", "Jeroen van Velthoven");
            employeeRepository.save(employee2);

            Holiday holiday2 = new Holiday("Winter", LocalDate.parse("2022-01-01"), LocalDate.parse("2022-01-31"), HolidayStatus.REQUESTED);
            holiday2.setEmployee(employee2);
            holidayRepository.save(holiday2);

            Holiday holiday3 = new Holiday("Summer", LocalDate.parse("2022-07-01"), LocalDate.parse("2022-07-31"), HolidayStatus.REQUESTED);
            holiday3.setEmployee(employee2);
            holidayRepository.save(holiday3);

            Employee employee3 = new Employee("klm000003", "Cliff Sestig");
            employeeRepository.save(employee3);

            Holiday holiday4 = new Holiday("Summer", LocalDate.parse("2022-06-01"), LocalDate.parse("2022-06-30"), HolidayStatus.REQUESTED);
            holiday4.setEmployee(employee3);
            holidayRepository.save(holiday4);

            log.info("Finished inserting data");
        };
    }


}
